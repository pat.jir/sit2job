package com.sit.sit2job.Student.Form;

public class ApplyJobForm {

    private Integer jobId;

    public Integer getJobId() {
        return jobId;
    }

    public void setJobId(Integer jobId) {
        this.jobId = jobId;
    }
}
