package com.sit.sit2job.Company.Entity;

import com.sit.sit2job.Student.Entity.ApplyJobEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "job", schema = "sit2job")
public class JobEntity {
    private int jobId;
    private String benefit;
    private int candidateAmount;
    private String description;
    private Timestamp expiredate;
    private String extraInformation;
    private int isActive;
    private int isSuspend;
    private int isWaitPost;
    private String qualification;
    private Timestamp startdate;
    private Timestamp timestamp;
    private String title;
    private int isDisable;
    private List<ApplyJobEntity> applyJob;
    private CompanyEntity companyId;
    private JobTypeEntity jobTypeEntity;
    private JobFieldEntity jobFieldEntity;
    private List<JobCompetenciesEntity> jobCompetencies;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "job_id", nullable = false)
    public int getJobId() {
        return jobId;
    }

    public void setJobId(int jobId) {
        this.jobId = jobId;
    }

    @Basic
    @Column(name = "benefit", nullable = false, length = 500)
    public String getBenefit() {
        return benefit;
    }

    public void setBenefit(String benefit) {
        this.benefit = benefit;
    }

    @Basic
    @Column(name = "candidate_amount", nullable = false)
    public int getCandidateAmount() {
        return candidateAmount;
    }

    public void setCandidateAmount(int candidateAmount) {
        this.candidateAmount = candidateAmount;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "expiredate", nullable = false)
    public Timestamp getExpiredate() {
        return expiredate;
    }

    public void setExpiredate(Timestamp expiredate) {
        this.expiredate = expiredate;
    }

    @Basic
    @Column(name = "extra_information", nullable = false, length = 500)
    public String getExtraInformation() {
        return extraInformation;
    }

    public void setExtraInformation(String extraInformation) {
        this.extraInformation = extraInformation;
    }

    @Basic
    @Column(name = "is_active", nullable = false)
    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    @Basic
    @Column(name = "is_suspend", nullable = false)
    public int getIsSuspend() {
        return isSuspend;
    }

    public void setIsSuspend(int isSuspend) {
        this.isSuspend = isSuspend;
    }

    @Basic
    @Column(name = "is_wait_post", nullable = false)
    public int getIsWaitPost() {
        return isWaitPost;
    }

    public void setIsWaitPost(int isWaitPost) {
        this.isWaitPost = isWaitPost;
    }

    @Basic
    @Column(name = "qualification", nullable = false, length = 500)
    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    @Basic
    @Column(name = "startdate", nullable = false)
    public Timestamp getStartdate() {
        return startdate;
    }

    public void setStartdate(Timestamp startdate) {
        this.startdate = startdate;
    }

    @Basic
    @Column(name = "title", nullable = false, length = 100)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @OneToMany(mappedBy = "jobEntity", cascade = CascadeType.ALL)
    public List<JobCompetenciesEntity> getJobCompetencies() {
        return jobCompetencies;
    }

    public void setJobCompetencies(List<JobCompetenciesEntity> jobCompetencies) {
        this.jobCompetencies = jobCompetencies;
    }

    @ManyToOne
    @JoinColumn(name="job_type_id")
    public JobTypeEntity getJobTypeEntity() {
        return jobTypeEntity;
    }

    public void setJobTypeEntity(JobTypeEntity jobTypeEntity) {
        this.jobTypeEntity = jobTypeEntity;
    }

    @ManyToOne
    @JoinColumn(name="job_field_id")
    public JobFieldEntity getJobFieldEntity() {
        return jobFieldEntity;
    }

    public void setJobFieldEntity(JobFieldEntity jobFieldEntity) {
        this.jobFieldEntity = jobFieldEntity;
    }

    @ManyToOne
    @JoinColumn(name="company_id")
    public CompanyEntity getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyEntity companyId) {
        this.companyId = companyId;
    }

    @OneToMany(mappedBy = "job")
    @Where(clause="is_disable = 0")
    public List<ApplyJobEntity> getApplyJob() {
        return applyJob;
    }

    public void setApplyJob(List<ApplyJobEntity> applyJob) {
        this.applyJob = applyJob;
    }

//    @OneToMany(mappedBy = "job")
//    public List<ApplyJobEntity> getApplyJobIgnoreCaseDisable() {
//        return applyJob;
//    }
//
//    public void setApplyJobIgnoreCaseDisable(List<ApplyJobEntity> applyJob) {
//        this.applyJob = applyJob;
//    }

    @Basic
    @Column(name="is_disable")
    public int getIsDisable() {
        return isDisable;
    }

    public void setIsDisable(int isDisable) {
        this.isDisable = isDisable;
    }

    @Basic
    @Column(name="timestamp")
    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobEntity jobEntity = (JobEntity) o;
        return jobId == jobEntity.jobId &&
                candidateAmount == jobEntity.candidateAmount &&
                companyId == jobEntity.companyId &&
                isActive == jobEntity.isActive &&
                isSuspend == jobEntity.isSuspend &&
                isWaitPost == jobEntity.isWaitPost &&
                Objects.equals(benefit, jobEntity.benefit) &&
                Objects.equals(description, jobEntity.description) &&
                Objects.equals(expiredate, jobEntity.expiredate) &&
                Objects.equals(extraInformation, jobEntity.extraInformation) &&
                Objects.equals(qualification, jobEntity.qualification) &&
                Objects.equals(startdate, jobEntity.startdate) &&
                Objects.equals(title, jobEntity.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobId, benefit, candidateAmount, companyId, description, expiredate, extraInformation, isActive, isSuspend, isWaitPost, qualification, startdate, title);
    }

    @Override
    public String toString() {
        return "Title: "+title + "\n"+
                "Description: "+description + "\n"+
                "Qualification: "+qualification + "\n"+
                "Benefit: "+benefit;
    }
}
