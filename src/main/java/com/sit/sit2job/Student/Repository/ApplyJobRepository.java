package com.sit.sit2job.Student.Repository;

import com.sit.sit2job.Company.Entity.JobEntity;
import com.sit.sit2job.Student.Entity.ApplyJobEntity;
import com.sit.sit2job.User.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ApplyJobRepository extends JpaRepository<ApplyJobEntity, Integer> {

    public ApplyJobEntity findOneByUserAndJobAndIsDisable(UserEntity user, JobEntity job, int isDisable);

    public List<ApplyJobEntity> findAllByJobAndIsDisable(JobEntity job, int isDisable);

    public List<ApplyJobEntity> findAllByUserAndIsDisable(UserEntity user, int isDisable);


}
