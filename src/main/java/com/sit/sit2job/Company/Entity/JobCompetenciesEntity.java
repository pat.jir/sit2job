package com.sit.sit2job.Company.Entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "job_competencies", schema = "sit2job")
public class JobCompetenciesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int jobCompetenciesId;
    private double weightScore;

    @ManyToOne
    @JoinColumn(name="job_id")
    private JobEntity jobEntity;

    @ManyToOne
    @JoinColumn(name="competencies_id")
    private CompetenciesEntity competenciesEntity;


    @Column(name = "job_competencies_id", nullable = false)
    public Integer getJobCompetenciesId() {
        return jobCompetenciesId;
    }

    public void setJobCompetenciesId(Integer jobCompetenciesId) {
        this.jobCompetenciesId = jobCompetenciesId;
    }

    public void setJobCompetenciesId(int jobCompetenciesId) {
        this.jobCompetenciesId = jobCompetenciesId;
    }

    public JobEntity getJobEntity() {
        return jobEntity;
    }

    public void setJobEntity(JobEntity jobEntity) {
        this.jobEntity = jobEntity;
    }

    public CompetenciesEntity getCompetenciesEntity() {
        return competenciesEntity;
    }

    public void setCompetenciesEntity(CompetenciesEntity competenciesEntity) {
        this.competenciesEntity = competenciesEntity;
    }

    @Basic
    @Column(name = "weight_score", nullable = false, precision = 0)
    public double getWeightScore() {
        return weightScore;
    }

    public void setWeightScore(double weightScore) {
        this.weightScore = weightScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobCompetenciesEntity that = (JobCompetenciesEntity) o;
        return jobCompetenciesId == that.jobCompetenciesId &&
                Double.compare(that.weightScore, weightScore) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobCompetenciesId, weightScore);
    }
}
