package com.sit.sit2job.Student.Entity;

import com.sit.sit2job.User.Entity.UserEntity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "student", schema = "sit2job")
public class StudentEntity {
    private int studentId;
    private Integer availableStatus;
    private Date dob;
    private String email;
    private String facebook;
    private String firstname;
    private String gender;
    private Double gpa;
    private Integer interviewAmount;
    private String lastname;
    private String lineId;
    private Integer militaryStatus;
    private Integer notification;
    private String phoneNo;
    private Integer probationStatus;
    private String program;
    private String resume;
    private String transcript;
    private UserEntity user;
    private YearEntity year;
    private DepartmentEntity department;
    private String address;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "student_id", nullable = false)
    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    @Basic
    @Column(name = "available_status", nullable = true)
    public Integer getAvailableStatus() {
        return availableStatus;
    }

    public void setAvailableStatus(Integer availableStatus) {
        this.availableStatus = availableStatus;
    }

    @Basic
    @Column(name = "dob", nullable = true)
    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    @Basic
    @Column(name = "email", nullable = true, length = 100)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "facebook", nullable = true, length = 50)
    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    @Basic
    @Column(name = "firstname", nullable = true, length = 50)
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    @Basic
    @Column(name = "gender", nullable = true, length = 10)
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    @Basic
    @Column(name = "gpa", nullable = true, precision = 0)
    public Double getGpa() {
        return gpa;
    }

    public void setGpa(Double gpa) {
        this.gpa = gpa;
    }

    @Basic
    @Column(name = "interview_amount", nullable = true)
    public Integer getInterviewAmount() {
        return interviewAmount;
    }

    public void setInterviewAmount(Integer interviewAmount) {
        this.interviewAmount = interviewAmount;
    }

    @Basic
    @Column(name = "lastname", nullable = true, length = 50)
    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Basic
    @Column(name = "line_id", nullable = true, length = 20)
    public String getLineId() {
        return lineId;
    }

    public void setLineId(String lineId) {
        this.lineId = lineId;
    }

    @Basic
    @Column(name = "military_status", nullable = true)
    public Integer getMilitaryStatus() {
        return militaryStatus;
    }

    public void setMilitaryStatus(Integer militaryStatus) {
        this.militaryStatus = militaryStatus;
    }

    @Basic
    @Column(name = "notification", nullable = true)
    public Integer getNotification() {
        return notification;
    }

    public void setNotification(Integer notification) {
        this.notification = notification;
    }

    @Basic
    @Column(name = "phone_no", nullable = true, length = 10)
    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Basic
    @Column(name = "probation_status", nullable = true)
    public Integer getProbationStatus() {
        return probationStatus;
    }

    public void setProbationStatus(Integer probationStatus) {
        this.probationStatus = probationStatus;
    }

    @Basic
    @Column(name = "program", nullable = true, length = 50)
    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    @Basic
    @Column(name = "resume", nullable = true, length = 200)
    public String getResume() {
        return resume;
    }

    public void setResume(String resume) {
        this.resume = resume;
    }

    @Basic
    @Column(name = "transcript", nullable = true, length = 200)
    public String getTranscript() {
        return transcript;
    }

    public void setTranscript(String transcript) {
        this.transcript = transcript;
    }

    @Basic
    @Column(name="address")
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @ManyToOne
    @JoinColumn(name="department_id")
    public DepartmentEntity getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentEntity department) {
        this.department = department;
    }

    @ManyToOne
    @JoinColumn(name="year_id")
    public YearEntity getYear() {
        return year;
    }

    public void setYear(YearEntity year) {
        this.year = year;
    }

    @OneToOne
    @JoinColumn(name="user_id")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StudentEntity that = (StudentEntity) o;
        return studentId == that.studentId &&
                Objects.equals(availableStatus, that.availableStatus) &&
                Objects.equals(dob, that.dob) &&
                Objects.equals(email, that.email) &&
                Objects.equals(facebook, that.facebook) &&
                Objects.equals(firstname, that.firstname) &&
                Objects.equals(gender, that.gender) &&
                Objects.equals(gpa, that.gpa) &&
                Objects.equals(interviewAmount, that.interviewAmount) &&
                Objects.equals(lastname, that.lastname) &&
                Objects.equals(lineId, that.lineId) &&
                Objects.equals(militaryStatus, that.militaryStatus) &&
                Objects.equals(notification, that.notification) &&
                Objects.equals(phoneNo, that.phoneNo) &&
                Objects.equals(probationStatus, that.probationStatus) &&
                Objects.equals(program, that.program) &&
                Objects.equals(resume, that.resume) &&
                Objects.equals(transcript, that.transcript);
    }

    @Override
    public int hashCode() {
        return Objects.hash(studentId, availableStatus, dob, email, facebook, firstname, gender, gpa, interviewAmount, lastname, lineId, militaryStatus, notification, phoneNo, probationStatus, program, resume, transcript);
    }
}
