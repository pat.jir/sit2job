package com.sit.sit2job.Company.Repository;

import com.sit.sit2job.Company.Entity.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompanyRepository extends JpaRepository<CompanyEntity, Integer> {

    public CompanyEntity findOneByUserId(int id);

    public CompanyEntity findOneByCompanyId(int id);
}
