package com.sit.sit2job;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.sit.sit2job.Company.Entity",
        "com.sit.sit2job.Company.Repository",
        "com.sit.sit2job.Company.Controller",
        "com.sit.sit2job.Company.Service",
        "com.sit.sit2job.Component",
        "com.sit.sit2job.Company.Configuration",
        "com.sit.sit2job.Company.POJO",
        "com.sit.sit2job.User.Repository",
        "com.sit.sit2job.User",
        "com.sit.sit2job.Student",
		"com.sit.sit2job.Student.Service"})
public class Sit2jobApplication {

	public static void main(String[] args) {
		SpringApplication.run(Sit2jobApplication.class, args);
	}
}
