package com.sit.sit2job.Company.Repository;

import com.sit.sit2job.Company.Entity.JobTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobTypeRepository extends JpaRepository<JobTypeEntity, Integer> {

    public List<JobTypeEntity> findAllByOrderByJobTypeIdAsc();
}
