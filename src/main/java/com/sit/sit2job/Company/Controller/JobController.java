package com.sit.sit2job.Company.Controller;

import com.sit.sit2job.Company.Entity.*;
import com.sit.sit2job.Company.Form.JobCreateForm;
import com.sit.sit2job.Company.POJO.WeightJobCompetency;
import com.sit.sit2job.Company.POJO.JobCreationDetail;
import com.sit.sit2job.Company.Repository.*;
import com.sit.sit2job.Company.Service.JobService;
import com.sit.sit2job.Student.Entity.ApplyJobEntity;
import com.sit.sit2job.Student.Entity.StudentEntity;
import com.sit.sit2job.Student.Repository.ApplyJobRepository;
import com.sit.sit2job.Student.Repository.StudentRepository;
import com.sit.sit2job.User.Entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping(value = "/job")
public class JobController {

    @Autowired
    JobRepository jobRepository;

    @Autowired
    JobTypeRepository jobTypeRepository;

    @Autowired
    JobFieldRepository jobFieldRepository;

    @Autowired
    CompetenciesRepository competenciesRepository;

    @Autowired
    JobService jobService;

    @Autowired
    JobCreationDetail jobCreationDetail;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ApplyJobRepository applyJobRepository;

    @GetMapping(value="/")
    public ModelAndView index(ModelAndView modelAndView, Principal principal){
        UserEntity user = userRepository.findOneByUsername(principal.getName());
        List<JobEntity> jobs = jobRepository.findAllByCompanyIdAndIsDisable
                (user.getCompanyId(),0);
        modelAndView.addObject("jobs", jobs);
        modelAndView.setViewName("/company/job");
        return modelAndView;
    }

    @GetMapping(value = "/detail")
    public ModelAndView jobDetail(@RequestParam("jobId") int jobId, ModelAndView modelAndView, Principal principal){
        System.out.println(jobId);
        JobEntity job = jobRepository.findOneByJobIdAndIsDisable(jobId,0);
        UserEntity user = userRepository.findOneByUsername(principal.getName());
        modelAndView.addObject("job", job);
        modelAndView.addObject("company",user.getCompanyId());
        modelAndView.setViewName("/company/job_detail");
        return modelAndView;
    }

    @GetMapping(value = "/createJob1")
    public ModelAndView createJob1(ModelAndView modelAndView){
        List<JobTypeEntity> jobTypeList = jobTypeRepository.findAllByOrderByJobTypeIdAsc();
        List<JobFieldEntity> jobFieldList = jobFieldRepository.findAllByOrderByJobFieldIdAsc();
        List<CompetenciesEntity> competenciesList = competenciesRepository.findAllByOrderByCompetenciesIdAsc();
        modelAndView.setViewName("/company/createJob_1");
        modelAndView.addObject("jobType",jobTypeList);
        modelAndView.addObject("jobField",jobFieldList);
        modelAndView.addObject("competency",competenciesList);
        return modelAndView;
    }

    @PostMapping(value = "/saveJob")
    public Object saveJob(JobCreateForm form, Principal principal) throws NoSuchAlgorithmException {
        // Create Method get JobId by name and timeStamp
        jobCreationDetail = jobService.createJob(form,principal);
        return "redirect:/job/createJob2";
    }

    @GetMapping(value = "/createJob2")
    public ModelAndView createJob2(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("/company/createJob_2");
        modelAndView.addObject(new WeightJobCompetency());
        modelAndView.addObject("jobCompetencies",jobCreationDetail.getJobCompetenciesEntityMap());
        modelAndView.addObject("jobTitle", jobCreationDetail.getJobEntity().getTitle());
        return modelAndView;
    }

    //BindingResult
    @PostMapping(value="/finishCreateJob")
    public Object setScoreJobCompetency(WeightJobCompetency weightJobCompetency){
        jobService.setScore(weightJobCompetency,jobCreationDetail);
        return "redirect:/job/";
    }

    @GetMapping(value="/candidate_list")
    public ModelAndView watchCandidate(@RequestParam int jobId ,ModelAndView modelAndView){
        JobEntity job = jobRepository.findOneByJobIdAndIsDisable(jobId,0);
        List<ApplyJobEntity> applyJobList = applyJobRepository.findAllByJobAndIsDisable(job,0);
        modelAndView.addObject("candidate", applyJobList);
        modelAndView.addObject("jobId",jobId);
        modelAndView.setViewName("/company/candidate_list");
        return modelAndView;
    }

    @GetMapping(value = "/student_detail")
    public ModelAndView studentDetail(@RequestParam int studentId,
                                      @RequestParam int interviewStatus,
                                      @RequestParam int jobId,
                                      ModelAndView modelAndView){
        UserEntity user = userRepository.findOneByUserId(studentId);
        Date date = user.getStudentEntity().getDob();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        LocalDate localDate = ((java.sql.Date) date).toLocalDate();
        String dob = formatter.format(date);
        String age = "" + Period.between(localDate, LocalDate.now()).getYears();
        modelAndView.addObject("student",user);
        modelAndView.addObject("interviewStatus",interviewStatus);
        modelAndView.addObject("jobId",jobId);
        modelAndView.addObject("dob", dob);
        modelAndView.addObject("age", age);
        modelAndView.setViewName("/company/student_detail");
        return modelAndView;
    }

    @PostMapping(value = "/confirm_student")
    public Object setInterviewStatus(@RequestParam int userId,
                                     @RequestParam int jobId){
        UserEntity user = userRepository.findOneByUserId(userId);
        JobEntity job = jobRepository.findOneByJobIdAndIsDisable(jobId,0);
        ApplyJobEntity applyJob = applyJobRepository.findOneByUserAndJobAndIsDisable(user,job,0);
        if (applyJob.getInterviewStatus() == 0){
            applyJob.setInterviewStatus(1);
            applyJobRepository.save(applyJob);
        }
        return "redirect:/job/student_detail?studentId="+userId+
                "&interviewStatus="+applyJob.getInterviewStatus()+
                "&jobId="+jobId;
    }

}
