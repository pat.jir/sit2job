package com.sit.sit2job.Company.Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "competencies", schema = "sit2job")
public class CompetenciesEntity {

    private int competenciesId;
    private String description;
    private String name;
    private List<JobCompetenciesEntity> jobCompetenciesEntity;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "competencies_id", nullable = false)
    public int getCompetenciesId() {
        return competenciesId;
    }

    public void setCompetenciesId(int competenciesId) {
        this.competenciesId = competenciesId;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 40)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 30)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "competenciesEntity")
    public List<JobCompetenciesEntity> getJobCompetenciesEntity() {
        return jobCompetenciesEntity;
    }

    public void setJobCompetenciesEntity(List<JobCompetenciesEntity> jobCompetenciesEntity) {
        this.jobCompetenciesEntity = jobCompetenciesEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompetenciesEntity that = (CompetenciesEntity) o;
        return competenciesId == that.competenciesId &&
                Objects.equals(description, that.description) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(competenciesId, description, name);
    }
}
