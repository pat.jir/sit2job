package com.sit.sit2job.Company.Service;

import com.sit.sit2job.Company.Form.CompanyAddForm;
import com.sit.sit2job.Company.Repository.CompanyRepository;
import com.sit.sit2job.Company.Repository.UserRepository;
import com.sit.sit2job.Component.PasswordEncoder;
import com.sit.sit2job.Company.Entity.CompanyEntity;
import com.sit.sit2job.User.Entity.RoleEntity;
import com.sit.sit2job.User.Entity.UserEntity;
import com.sit.sit2job.User.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;

@Service
public class CompanyService extends CompanyAppService {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    CompanyRepository companyRepository;

    public void createCompany(CompanyAddForm companyAddForm) throws NoSuchAlgorithmException {
        UserEntity user = new UserEntity();
        CompanyEntity company = new CompanyEntity();
        RoleEntity role = roleRepository.findOneByRoleName("company");
        user.setPassword(passwordEncoder.hashMD5(companyAddForm.getPassword()));
        user.setUsername(companyAddForm.getUsername());
        user.setRoleId(role);
        company.setEmail(companyAddForm.getEmail());
        company.setPhoneNo(companyAddForm.getPhoneNo());
        company.setName(companyAddForm.getName());
        company.setEmail(companyAddForm.getEmail());
        company.setAddress(companyAddForm.getAddress());
        company.setContactPersonName(companyAddForm.getContactPersonName());
        company.setDetail(companyAddForm.getDetail());
        company.setDescription(companyAddForm.getDescription());
        if(!companyAddForm.getFaxNo().isEmpty()){
            company.setFaxNo(companyAddForm.getFaxNo());
        }
        if(!companyAddForm.getWebsite().isEmpty()){
            company.setWebsite(companyAddForm.getWebsite());
        }
        company.setUserId(user);
        user.setCompanyId(company);
        userRepository.save(user);
    }

}