package com.sit.sit2job.Company.Service;

import com.sit.sit2job.Company.Repository.UserRepository;
import com.sit.sit2job.User.Entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Arrays;
@Service
public class AuthenticationService implements UserDetailsService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findOneByUsername(username);
        GrantedAuthority authority = new SimpleGrantedAuthority(userEntity.getRoleId().getRoleName());
        boolean isEnabled = true;
        boolean isAccountNonExpired = true;
        boolean isCredentialsNonExpired = true;
        boolean isAccountNonLocked = true;
        UserDetails userDetails = (UserDetails) new
                org.springframework.security.core.userdetails.User(userEntity.getUsername(),
                userEntity.getPassword(), isEnabled, isAccountNonExpired, isCredentialsNonExpired,
                isAccountNonLocked, Arrays.asList(authority));
        return userDetails;
    }
}
