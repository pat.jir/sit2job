package com.sit.sit2job.Company.POJO;

import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import java.util.Map;

public class TestForm {

    @NotNull
    @Range(min = 1, max = 10)
    private Integer x;

    public Integer getX() {
        return x;
    }

    public void setX(Integer x) {
        this.x = x;
    }

    private Map<String, Integer> weightScore;

    public Map<String, Integer> getWeightScore() {
        return weightScore;
    }

    public void setWeightScore(Map<String, Integer> weightScore) {
        this.weightScore = weightScore;
    }
}
