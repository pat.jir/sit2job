package com.sit.sit2job.Company.Repository;

import com.sit.sit2job.Company.Entity.JobCompetenciesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobCompetenciesRepository extends JpaRepository<JobCompetenciesEntity, Integer> {
}
