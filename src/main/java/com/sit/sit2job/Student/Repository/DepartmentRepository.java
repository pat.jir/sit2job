package com.sit.sit2job.Student.Repository;

import com.sit.sit2job.Student.Entity.DepartmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartmentRepository extends JpaRepository<DepartmentEntity, Integer> {

    public DepartmentEntity findOneByDepartmentId(int id);

    public DepartmentEntity findOneByDepartmentName(String name);
}
