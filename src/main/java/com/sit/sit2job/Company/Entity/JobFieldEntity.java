package com.sit.sit2job.Company.Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "job_field", schema = "sit2job", catalog = "")
public class JobFieldEntity {
    private int jobFieldId;
    private String jobFieldName;
    private List<JobEntity> jobEntity;

    @Id
    @Column(name = "job_field_id", nullable = false)
    public int getJobFieldId() {
        return jobFieldId;
    }

    public void setJobFieldId(int jobFieldId) {
        this.jobFieldId = jobFieldId;
    }

    @Basic
    @Column(name = "job_field_name", nullable = false, length = 100)
    public String getJobFieldName() {
        return jobFieldName;
    }

    public void setJobFieldName(String jobFieldName) {
        this.jobFieldName = jobFieldName;
    }

    @OneToMany(mappedBy = "jobFieldEntity")
    public List<JobEntity> getJobEntity() {
        return jobEntity;
    }

    public void setJobEntity(List<JobEntity> jobEntity) {
        this.jobEntity = jobEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobFieldEntity that = (JobFieldEntity) o;
        return jobFieldId == that.jobFieldId &&
                Objects.equals(jobFieldName, that.jobFieldName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobFieldId, jobFieldName);
    }
}
