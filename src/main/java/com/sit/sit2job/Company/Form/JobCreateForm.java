package com.sit.sit2job.Company.Form;

public class JobCreateForm {

    private String jobTitle;

    public String getJobField() {
        return jobField;
    }

    public void setJobField(String jobField) {
        this.jobField = jobField;
    }

    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    private String jobField;
    private String jobType;
    private String[] competencyCheckbox;
    private String jobDescription;
    private String jobQualification;
    private String jobBenefit;
    private String weight;
    private String extraDescription;

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public String[] getCompetencyCheckbox() {
        return competencyCheckbox;
    }

    public void setCompetencyCheckbox(String[] competencyCheckbox) {
        this.competencyCheckbox = competencyCheckbox;
    }

    public String getJobDescription() {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription) {
        this.jobDescription = jobDescription;
    }

    public String getJobQualification() {
        return jobQualification;
    }

    public void setJobQualification(String jobQualification) {
        this.jobQualification = jobQualification;
    }

    public String getJobBenefit() {
        return jobBenefit;
    }

    public void setJobBenefit(String jobBenefit) {
        this.jobBenefit = jobBenefit;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getExtraDescription() {
        return extraDescription;
    }

    public void setExtraDescription(String extraDescription) {
        this.extraDescription = extraDescription;
    }
}
