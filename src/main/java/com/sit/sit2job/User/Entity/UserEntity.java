package com.sit.sit2job.User.Entity;

import com.sit.sit2job.Company.Entity.CompanyEntity;
import com.sit.sit2job.Student.Entity.ApplyJobEntity;
import com.sit.sit2job.Student.Entity.StudentEntity;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "sit2job")
public class UserEntity {
    private int userId;
    private String username;
    private String password;
    private RoleEntity roleId;
    private CompanyEntity companyId;
    private StudentEntity studentEntity;
    private List<ApplyJobEntity> applyJob;

    @OneToMany(mappedBy = "user")
    @Where(clause = "is_disable = 0")
    public List<ApplyJobEntity> getApplyJob() {
        return applyJob;
    }

    public void setApplyJob(List<ApplyJobEntity> applyJob) {
        this.applyJob = applyJob;
    }

//    @OneToMany(mappedBy = "user")
//    public List<ApplyJobEntity> getApplyJobIgnoreDisable() {
//        return applyJob;
//    }
//
//    public void setApplyJobIgnoreDisable(List<ApplyJobEntity> applyJob) {
//        this.applyJob = applyJob;
//    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "user_id", nullable = false)
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 50)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 200)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @ManyToOne
    @JoinColumn(name="role_id")
    public RoleEntity getRoleId() {
        return roleId;
    }

    public void setRoleId(RoleEntity roleId) {
        this.roleId = roleId;
    }

    @OneToOne(mappedBy = "userId", cascade = CascadeType.ALL)
    public CompanyEntity getCompanyId() {
        return companyId;
    }

    public void setCompanyId(CompanyEntity companyId) {
        this.companyId = companyId;
    }

    @OneToOne(mappedBy = "user")
    public StudentEntity getStudentEntity() {
        return studentEntity;
    }

    public void setStudentEntity(StudentEntity studentEntity) {
        this.studentEntity = studentEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return userId == that.userId &&
                Objects.equals(username, that.username) &&
                Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, username, password);
    }
}
