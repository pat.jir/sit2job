package com.sit.sit2job.Company.POJO;

import com.sit.sit2job.Company.Entity.JobCompetenciesEntity;
import com.sit.sit2job.Company.Entity.JobEntity;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Map;

@Component
public class JobCreationDetail {
    private JobEntity jobEntity;
    private Map<String, JobCompetenciesEntity> jobCompetenciesEntityMap;
    private List<JobCompetenciesEntity> jobCompetenciesEntityList;

    public JobEntity getJobEntity() {
        return jobEntity;
    }

    public void setJobEntity(JobEntity jobEntity) {
        this.jobEntity = jobEntity;
    }

    public Map<String, JobCompetenciesEntity> getJobCompetenciesEntityMap() {
        return jobCompetenciesEntityMap;
    }

    public void setJobCompetenciesEntityMap(Map<String, JobCompetenciesEntity> jobCompetenciesEntityMap) {
        this.jobCompetenciesEntityMap = jobCompetenciesEntityMap;
    }

    public List<JobCompetenciesEntity> getJobCompetenciesEntityList() {
        return jobCompetenciesEntityList;
    }

    public void setJobCompetenciesEntityList(List<JobCompetenciesEntity> jobCompetenciesEntityList) {
        this.jobCompetenciesEntityList = jobCompetenciesEntityList;
    }
}
