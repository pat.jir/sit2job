package com.sit.sit2job.Company.Repository;

import com.sit.sit2job.Company.Entity.CompetenciesEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CompetenciesRepository extends JpaRepository<CompetenciesEntity, Integer> {

    public List<CompetenciesEntity> findAllByOrderByCompetenciesIdAsc();

    public CompetenciesEntity findOneByCompetenciesId(int id);
}
