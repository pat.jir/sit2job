package com.sit.sit2job.Student.Entity;

import com.sit.sit2job.Company.Entity.JobEntity;
import com.sit.sit2job.User.Entity.UserEntity;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "apply_job", schema = "sit2job")
public class ApplyJobEntity {
    private int applyJobId;
    private Timestamp datetime;
    private int interviewStatus;
    private UserEntity user;
    private JobEntity job;
    private int isDisable;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "apply_job_id", nullable = false)
    public int getApplyJobId() {
        return applyJobId;
    }

    public void setApplyJobId(int applyJobId) {
        this.applyJobId = applyJobId;
    }

    @Basic
    @Column(name = "datetime", nullable = false)
    public Timestamp getDatetime() {
        return datetime;
    }

    public void setDatetime(Timestamp datetime) {
        this.datetime = datetime;
    }

    @Basic
    @Column(name = "interview_status", nullable = false)
    public int getInterviewStatus() {
        return interviewStatus;
    }

    public void setInterviewStatus(int interviewStatus) {
        this.interviewStatus = interviewStatus;
    }

    @Basic
    @Column(name="is_disable")
    public int getIsDisable() {
        return isDisable;
    }

    public void setIsDisable(int isDisable) {
        this.isDisable = isDisable;
    }

    @ManyToOne
    @JoinColumn(name="user_id")
    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    @ManyToOne
    @JoinColumn(name="job_id")
    public JobEntity getJob() {
        return job;
    }

    public void setJob(JobEntity job) {
        this.job = job;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ApplyJobEntity that = (ApplyJobEntity) o;
        return applyJobId == that.applyJobId &&
                interviewStatus == that.interviewStatus &&
                Objects.equals(datetime, that.datetime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(applyJobId, datetime, interviewStatus);
    }
}
