package com.sit.sit2job.Company.Repository;

import com.sit.sit2job.Company.Entity.CompanyEntity;
import com.sit.sit2job.Company.Entity.JobEntity;
import com.sit.sit2job.Company.Entity.JobFieldEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobRepository extends JpaRepository<JobEntity, Integer> {

    public JobEntity findOneByJobIdAndIsDisable(int jobId, int isDisable);

    public List<JobEntity> findAllByCompanyIdAndIsDisable(CompanyEntity id, int isDisable);

    public List<JobEntity> findAllByJobFieldEntity(JobFieldEntity jobFieldEntity);

//    List<JobEntity> findJobEntitiesByApplyJobIsDisable(int isDisable);

}
