package com.sit.sit2job.Student.Controller;

import com.sit.sit2job.Company.Entity.CompanyEntity;
import com.sit.sit2job.Company.Entity.JobEntity;
import com.sit.sit2job.Company.Repository.CompanyRepository;
import com.sit.sit2job.Company.Repository.JobRepository;
import com.sit.sit2job.Company.Repository.UserRepository;
import com.sit.sit2job.Student.Entity.ApplyJobEntity;
import com.sit.sit2job.Student.Entity.DepartmentEntity;
import com.sit.sit2job.Student.Entity.YearEntity;
import com.sit.sit2job.Student.Form.ApplyJobForm;
import com.sit.sit2job.Student.Form.CreateStudentForm;
import com.sit.sit2job.Student.Form.EditProfile;
import com.sit.sit2job.Student.Repository.ApplyJobRepository;
import com.sit.sit2job.Student.Repository.DepartmentRepository;
import com.sit.sit2job.Student.Repository.StudentRepository;
import com.sit.sit2job.Student.Repository.YearRepository;
import com.sit.sit2job.Student.Service.StudentService;
import com.sit.sit2job.User.Entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.jws.WebParam;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/student")
public class StudentController {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    StudentService studentService;

    @Autowired
    YearRepository yearRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    JobRepository jobRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    ApplyJobRepository applyJobRepository;

    @GetMapping("/")
    public ModelAndView studentIndex(ModelAndView modelAndView, Principal principal) {
        modelAndView.setViewName("/student/index");
        UserEntity user = userRepository.findOneByUsername(principal.getName());


        return modelAndView;
    }

    @GetMapping("/register")
    public ModelAndView register(ModelAndView modelAndView) {
        LocalDate date = LocalDate.now();
        List<YearEntity> yearList = yearRepository.findAll();
        List<DepartmentEntity> department = departmentRepository.findAll();
        modelAndView.addObject("yearList", yearList);
        modelAndView.addObject("date_now", date);
        modelAndView.addObject("department", department);
        modelAndView.setViewName("/student/register");
        return modelAndView;
    }

    @PostMapping("/sign_up")
    public Object signUp(CreateStudentForm createStudentForm, BindingResult bindingResult) throws NoSuchAlgorithmException {
        if (bindingResult.hasErrors()) {
            return "redirect:/student/register";
        }
        // Send form to service
        studentService.createStudent(createStudentForm);
        return "redirect:/login";
    }

    @GetMapping("/profile")
    public ModelAndView viewProfile(ModelAndView modelAndView, Principal principal) {
        UserEntity student = userRepository.findOneByUsername(principal.getName());
        Date date = student.getStudentEntity().getDob();
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        LocalDate localDate = ((java.sql.Date) date).toLocalDate();
        String dob = formatter.format(date);
        String age = "" + Period.between(localDate, LocalDate.now()).getYears();
        modelAndView.addObject("student", student);
        modelAndView.addObject("dob", dob);
        modelAndView.addObject("age", age);
        modelAndView.setViewName("/student/profile");
        return modelAndView;
    }

    @GetMapping("/job")
    public ModelAndView viewJobPool(ModelAndView modelAndView) {
        List<JobEntity> jobs = jobRepository.findAll();
        modelAndView.addObject("jobs", jobs);
        modelAndView.setViewName("/student/jobPool");
        return modelAndView;
    }

    @GetMapping("/job_detail")
    public ModelAndView viewJobDetail(@RequestParam int jobId, ModelAndView modelAndView, Principal principal) {
        JobEntity job = jobRepository.findOneByJobIdAndIsDisable(jobId,0);
        UserEntity userEntity = userRepository.findOneByUsername(principal.getName());
        CompanyEntity companyEntity = companyRepository.findOneByCompanyId(job.getCompanyId().getCompanyId());
        ApplyJobEntity applyJob = applyJobRepository.findOneByUserAndJobAndIsDisable(userEntity,job,0);
        if(applyJob != null) {
            modelAndView.addObject("isApply", true);
            if(applyJob.getInterviewStatus() == 0)
                modelAndView.addObject("canCancel",true);
            else if(applyJob.getInterviewStatus() == 1)
                modelAndView.addObject("canCancel", false);
        }
        else
            modelAndView.addObject("isApply", false);
        modelAndView.addObject("job", job);
        modelAndView.addObject("company", companyEntity);
        modelAndView.setViewName("/student/job_detail");
        return modelAndView;
    }

    @PostMapping("/apply_job")
    public Object applyJob(@RequestParam int jobId, Principal principal) {
        String username = principal.getName();
        UserEntity user = userRepository.findOneByUsername(username);
        if (user.getStudentEntity().getAvailableStatus() == 1) {
            JobEntity job = jobRepository.findOneByJobIdAndIsDisable(jobId,0);
            ApplyJobEntity applyJob = new ApplyJobEntity();
            applyJob.setJob(job);
            applyJob.setUser(user);
            applyJob.setInterviewStatus(0);
            applyJobRepository.save(applyJob);
            return "redirect:/student/job_status";
        }
        return "redirect:/student/job";
    }

    @GetMapping("/job_status")
    public ModelAndView getApplyJobStatus(ModelAndView modelAndView, Principal principal) {
        UserEntity user = userRepository.findOneByUsername(principal.getName());
        List<ApplyJobEntity> applyJob = applyJobRepository.findAllByUserAndIsDisable(user,0);
        modelAndView.addObject("user",user);
        modelAndView.addObject("applyJob",applyJob);
        modelAndView.setViewName("/student/job_status");
        return modelAndView;
    }

    @PostMapping(value = "/cancel_apply_job")
    public Object cancelApplyJob(@RequestParam int jobId, Principal principal){
        UserEntity user = userRepository.findOneByUsername(principal.getName());
        JobEntity job = jobRepository.findOneByJobIdAndIsDisable(jobId,0);
        ApplyJobEntity applyJob = applyJobRepository.findOneByUserAndJobAndIsDisable(user,job,0);
        applyJob.setIsDisable(1);
        applyJobRepository.save(applyJob);
        return "redirect:/student/job_status";
    }

    @GetMapping(value = "/edit_profile")
    public ModelAndView editProfile(ModelAndView modelAndView, Principal principal){
        UserEntity user = userRepository.findOneByUsername(principal.getName());
        List<YearEntity> year = yearRepository.findAll();
        modelAndView.addObject("profile",user.getStudentEntity());
        modelAndView.addObject("yearList",year);
        modelAndView.setViewName("/student/edit_profile");
        return modelAndView;
    }

    @PostMapping(value="/save_profile")
    public Object saveProfile(EditProfile editProfile, Principal principal){
        studentService.editProfile(editProfile, principal);
        return "redirect:/student/profile";
    }

}
