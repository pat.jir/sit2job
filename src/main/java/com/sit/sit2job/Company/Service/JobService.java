package com.sit.sit2job.Company.Service;

import com.sit.sit2job.Company.Entity.*;
import com.sit.sit2job.Company.Form.JobCreateForm;
import com.sit.sit2job.Company.POJO.JobCreationDetail;
import com.sit.sit2job.Company.POJO.WeightJobCompetency;
import com.sit.sit2job.Company.Repository.*;
import com.sit.sit2job.Student.Entity.ApplyJobEntity;
import com.sit.sit2job.User.Entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.util.*;

@Service
public class JobService {

    @Autowired
    JobRepository jobRepository;

    @Autowired
    JobCompetenciesRepository jobCompetenciesRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    CompetenciesRepository competenciesRepository;

    @Autowired
    UserRepository userRepository;

    public JobCreationDetail createJob(JobCreateForm jobCreateForm, Principal principal) throws NoSuchAlgorithmException {
        JobCreationDetail jobCreationDetail = new JobCreationDetail();
        JobEntity jobEntity = new JobEntity();
        JobCompetenciesEntity[] jobCompetenciesEntities =
                new JobCompetenciesEntity[jobCreateForm.getCompetencyCheckbox().length];
        String[] checkBox = jobCreateForm.getCompetencyCheckbox();
        JobFieldEntity jobFieldEntity = new JobFieldEntity();
        JobTypeEntity jobTypeEntity = new JobTypeEntity();
        jobFieldEntity.setJobFieldId(Integer.parseInt(jobCreateForm.getJobField()));
        jobTypeEntity.setJobTypeId(Integer.parseInt(jobCreateForm.getJobType()));
        jobEntity.setTitle(jobCreateForm.getJobTitle());
        jobEntity.setJobFieldEntity(jobFieldEntity);
        jobEntity.setJobTypeEntity(jobTypeEntity);
        jobEntity.setDescription(jobCreateForm.getJobDescription());
        jobEntity.setBenefit(jobCreateForm.getJobBenefit());
        jobEntity.setQualification(jobCreateForm.getJobQualification());
        jobEntity.setExtraInformation(jobCreateForm.getExtraDescription());
        UserEntity userEntity = userRepository.findOneByUsername(principal.getName());
        jobEntity.setCompanyId(userEntity.getCompanyId());

        jobRepository.save(jobEntity);
        for(int i=0; i<jobCompetenciesEntities.length; i++){
            jobCompetenciesEntities[i] = new JobCompetenciesEntity();
            CompetenciesEntity competenciesEntity = new CompetenciesEntity();
            competenciesEntity.setCompetenciesId(Integer.parseInt(checkBox[i]));
            jobCompetenciesEntities[i].setCompetenciesEntity(competenciesEntity);
            jobCompetenciesEntities[i].setJobEntity(jobEntity);
            jobCompetenciesEntities[i].setWeightScore(0);
        }
        List<JobCompetenciesEntity> jobCompetenciesEntityList = new ArrayList<>();
        for (JobCompetenciesEntity job: jobCompetenciesEntities){
            jobCompetenciesRepository.save(job);
            jobCompetenciesEntityList.add(job);
        }
        jobCreationDetail.setJobCompetenciesEntityList(jobCompetenciesEntityList);
        jobCreationDetail.setJobEntity(jobEntity);
        jobCreationDetail.setJobCompetenciesEntityMap(setMap(jobCompetenciesEntities));
        return jobCreationDetail;
    }

    public void setScore(WeightJobCompetency weightJobCompetency, JobCreationDetail jobCreationDetail){
        Map<String, Integer> score = weightJobCompetency.getWeightScore();
        List<JobCompetenciesEntity> jobCompetenciesEntityList = jobCreationDetail.getJobCompetenciesEntityList();
        score.forEach((x,y) -> {
            for (JobCompetenciesEntity jobCompetenciesEntity: jobCompetenciesEntityList){
                if(jobCompetenciesEntity.getCompetenciesEntity().getCompetenciesId() == Integer.parseInt(x))
                {
                    jobCompetenciesEntity.setWeightScore(y);
                    jobCompetenciesRepository.save(jobCompetenciesEntity);
                }
            }
        });
    }

    private Map<String,JobCompetenciesEntity> setMap(JobCompetenciesEntity[] jobCompetenciesEntities){
        Map<String,JobCompetenciesEntity> map = new HashMap<>();
        for(JobCompetenciesEntity jobCompetencies: jobCompetenciesEntities){
            CompetenciesEntity competenciesEntity =
                    competenciesRepository.findOneByCompetenciesId(
                            jobCompetencies.getCompetenciesEntity().getCompetenciesId());
            map.put(competenciesEntity.getName(), jobCompetencies);
        }
        return map;
    }

    public List<JobEntity> deleteDisableApplyJob(List<JobEntity> jobs){
        for (JobEntity job: jobs){
            for (ApplyJobEntity applyJob: job.getApplyJob()){
                if(applyJob.getIsDisable() == 1)
                    job.getApplyJob().remove(applyJob);
            }
        }
        return jobs;
    }

}
