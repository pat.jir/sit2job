package com.sit.sit2job.Student.Repository;

import com.sit.sit2job.Student.Entity.YearEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface YearRepository extends JpaRepository<YearEntity, Integer> {

    public YearEntity findOneByYearId(int id);

}
