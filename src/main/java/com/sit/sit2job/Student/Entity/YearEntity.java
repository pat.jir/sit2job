package com.sit.sit2job.Student.Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "year", schema = "sit2job")
public class YearEntity {
    private int yearId;
    private int year;
    private List<StudentEntity> student;


    @Id
    @Column(name = "year_id", nullable = false)
    public int getYearId() {
        return yearId;
    }

    public void setYearId(int yearId) {
        this.yearId = yearId;
    }

    @Basic
    @Column(name = "year", nullable = false)
    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @OneToMany(mappedBy = "year")
    public List<StudentEntity> getStudent() {
        return student;
    }

    public void setStudent(List<StudentEntity> student) {
        this.student = student;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        YearEntity that = (YearEntity) o;
        return yearId == that.yearId &&
                year == that.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(yearId, year);
    }
}
