package com.sit.sit2job.Company.Repository;

import com.sit.sit2job.Company.Entity.JobFieldEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobFieldRepository extends JpaRepository<JobFieldEntity, Integer> {

    public List<JobFieldEntity> findAllByOrderByJobFieldIdAsc();
}
