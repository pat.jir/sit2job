package com.sit.sit2job.Company.Repository;

import com.sit.sit2job.User.Entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Integer> {

    public UserEntity findOneByUsername(String username);

    public UserEntity findOneByUserId(int id);
}
