package com.sit.sit2job.Company.Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import com.sit.sit2job.User.Entity.UserEntity;

@Entity
@Table(name = "company", schema = "sit2job")
public class CompanyEntity {

    private int companyId;
    private String address;
    private String contactPersonName;
    private String description;
    private String detail;
    private String email;
    private String faxNo;
    private String name;
    private String phoneNo;
    private String website;
    private UserEntity userId;
    private List<JobEntity> jobList;


    @OneToMany(mappedBy = "companyId")
    public List<JobEntity> getJobList() {
        return jobList;
    }

    public void setJobList(List<JobEntity> jobList) {
        this.jobList = jobList;
    }

    @OneToOne
    @JoinColumn(name="user_id")
    public UserEntity getUserId() {
        return userId;
    }

    public void setUserId(UserEntity userId) {
        this.userId = userId;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "company_id", nullable = false)
    public int getCompanyId() {
        return companyId;
    }

    public void setCompanyId(int companyId) {
        this.companyId = companyId;
    }

    @Basic
    @Column(name = "address", nullable = true, length = 500)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "contact_person_name", nullable = true, length = 500)
    public String getContactPersonName() {
        return contactPersonName;
    }

    public void setContactPersonName(String contactPersonName) {
        this.contactPersonName = contactPersonName;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "detail", nullable = true, length = 500)
    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    @Basic
    @Column(name = "email", nullable = false, length = 50)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "fax_no", nullable = true, length = 25)
    public String getFaxNo() {
        return faxNo;
    }

    public void setFaxNo(String faxNo) {
        this.faxNo = faxNo;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "phone_no", nullable = false, length = 25)
    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Basic
    @Column(name = "website", nullable = true, length = 100)
    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompanyEntity that = (CompanyEntity) o;
        return companyId == that.companyId &&
                Objects.equals(address, that.address) &&
                Objects.equals(contactPersonName, that.contactPersonName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(detail, that.detail) &&
                Objects.equals(email, that.email) &&
                Objects.equals(faxNo, that.faxNo) &&
                Objects.equals(name, that.name) &&
                Objects.equals(phoneNo, that.phoneNo) &&
                Objects.equals(website, that.website);
    }

    @Override
    public int hashCode() {
        return Objects.hash(companyId, address, contactPersonName, description, detail, email, faxNo, name, phoneNo, website);
    }
}
