package com.sit.sit2job.User.Repository;

import com.sit.sit2job.User.Entity.RoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Integer> {

    public RoleEntity findOneByRoleName(String roleName);
}
