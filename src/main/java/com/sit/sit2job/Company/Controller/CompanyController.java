package com.sit.sit2job.Company.Controller;

import com.sit.sit2job.Company.Form.CompanyAddForm;
import com.sit.sit2job.Company.Entity.CompanyEntity;
import com.sit.sit2job.Company.Repository.CompanyRepository;
import com.sit.sit2job.Company.Service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@Controller
@RequestMapping("/company")
public class CompanyController {

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyService companyService;

    @GetMapping(value="/all")
    public List<CompanyEntity> getAllCompany(){
        return companyRepository.findAll();
    }

    @PostMapping(value="/sign_up")
    public Object companySignUp(@Valid CompanyAddForm companyAddForm, BindingResult bindingResult, ModelAndView modelAndView) throws NoSuchAlgorithmException
    {
        modelAndView.addObject("signup",true);
        if(bindingResult.hasErrors()) {
            modelAndView.setViewName("/company/register");
            modelAndView.addObject("signup",false);
            return modelAndView;
        }
        companyService.createCompany(companyAddForm);
        return "redirect:/company/login";
    }

    @GetMapping(value="/register")
    public ModelAndView companyRegister(ModelAndView modelAndView){
        modelAndView.setViewName("/company/register");
        return modelAndView;
    }





}
