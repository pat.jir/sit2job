package com.sit.sit2job.Student.Repository;

import com.sit.sit2job.Student.Entity.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<StudentEntity, Integer> {

}
