package com.sit.sit2job.Student.POJO;

public class ReccomendJobField {
    private String jobFieldName;
    private Integer score;
    private Integer distance;

    public String getJobFieldName() {
        return jobFieldName;
    }

    public void setJobFieldName(String jobFieldName) {
        this.jobFieldName = jobFieldName;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }
}
