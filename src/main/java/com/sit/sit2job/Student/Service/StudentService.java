package com.sit.sit2job.Student.Service;

import com.sit.sit2job.Company.Entity.*;
import com.sit.sit2job.Company.Repository.JobCompetenciesRepository;
import com.sit.sit2job.Company.Repository.JobFieldRepository;
import com.sit.sit2job.Company.Repository.JobRepository;
import com.sit.sit2job.Company.Repository.UserRepository;
import com.sit.sit2job.Component.PasswordEncoder;
import com.sit.sit2job.Student.Entity.DepartmentEntity;
import com.sit.sit2job.Student.Entity.StudentEntity;
import com.sit.sit2job.Student.Entity.YearEntity;
import com.sit.sit2job.Student.Form.CreateStudentForm;
import com.sit.sit2job.Student.Form.EditProfile;
import com.sit.sit2job.Student.POJO.Competency;
import com.sit.sit2job.Student.POJO.ReccomendJobField;
import com.sit.sit2job.Student.POJO.SITCoinValue;
import com.sit.sit2job.Student.Repository.DepartmentRepository;
import com.sit.sit2job.Student.Repository.StudentRepository;
import com.sit.sit2job.Student.Repository.YearRepository;
import com.sit.sit2job.User.Entity.RoleEntity;
import com.sit.sit2job.User.Entity.UserEntity;
import com.sit.sit2job.User.Repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import java.time.Year;
import java.util.*;

@Service
public class StudentService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    YearRepository yearRepository;

    @Autowired
    DepartmentRepository departmentRepository;

    @Autowired
    JobFieldRepository jobFieldRepository;

    @Autowired
    JobRepository jobRepository;

    public void createStudent(CreateStudentForm createStudentForm) throws NoSuchAlgorithmException {
        PasswordEncoder passwordEncoder = new PasswordEncoder();
        RoleEntity role = roleRepository.findOneByRoleName("student");
        UserEntity user = new UserEntity();
        StudentEntity student = new StudentEntity();
        DepartmentEntity departmentEntity = departmentRepository.findOneByDepartmentId(createStudentForm.getDepartment());
        YearEntity year = yearRepository.findOneByYearId(createStudentForm.getYear());
        user.setRoleId(role);
        user.setUsername(createStudentForm.getUsername());
        user.setPassword(passwordEncoder.hashMD5(createStudentForm.getPassword()));
        student.setFirstname(createStudentForm.getName());
        student.setLastname(createStudentForm.getSurname());
        student.setYear(year);
        student.setDob(createStudentForm.getDob());
        student.setEmail(createStudentForm.getEmail());
        student.setDepartment(departmentEntity);
        student.setAvailableStatus(0);
        student.setGpa(createStudentForm.getGpa());
        student.setGender(createStudentForm.getGender());
        student.setPhoneNo(createStudentForm.getPhoneNo());
        student.setAddress(createStudentForm.getAddress());
        student.setMilitaryStatus(createStudentForm.getMilitary());
        user.setStudentEntity(student);
        userRepository.save(user);
        //student.setUser(user);
        //studentRepository.save(student);
    }

    public void editProfile(EditProfile editProfile, Principal principal){
        UserEntity user = userRepository.findOneByUsername(principal.getName());
        user.getStudentEntity().setFirstname(editProfile.getFirstname());
        user.getStudentEntity().setLastname(editProfile.getLastname());
        user.getStudentEntity().setMilitaryStatus(editProfile.getMilitary());
        user.getStudentEntity().setAddress(editProfile.getAddress());
        user.getStudentEntity().setGpa(editProfile.getGpa());
        user.getStudentEntity().setEmail(editProfile.getEmail());
        user.getStudentEntity().setLineId(editProfile.getLineId());
        user.getStudentEntity().setFacebook(editProfile.getFacebook());
        if (editProfile.getAvailableStatus() != null){
            user.getStudentEntity().setAvailableStatus(editProfile.getAvailableStatus());
        }
        // ขาด Proficiency
        userRepository.save(user);
    }

    public List<ReccomendJobField> reccomendationFeature(SITCoinValue studentSitCoin){
        List<Competency> studentCompetencies = setStudentCompetencies(studentSitCoin);
        List<JobFieldEntity> jobFieldList = jobFieldRepository.findAll();
        Map<String,List<Competency>> competencyAndScore = listCompetencyAndScore(jobFieldList);
        competencyAndScore = normalizeToPercent(competencyAndScore);

        return null;
    }

    private List<Competency> setStudentCompetencies(SITCoinValue studentSitCoin){
        List<Competency> studentCompetencies = new ArrayList<>();
        studentSitCoin.getCompetencies().forEach((k,v) -> {
            Competency competency = new Competency();
            competency.setCompetencyName(k);
            competency.setScore(v);
            studentCompetencies.add(competency);
        });
        return null;
    }

    private List<ReccomendJobField> euclidean (List<Competency> student, Map<String,List<Competency>> jobField){
        List<ReccomendJobField> reccomendJobFieldList = new ArrayList<>();
        jobField.forEach((k,v)->{
            for(Competency competency:v){
                Integer index = findPositionStudentCompetency(student,competency.getCompetencyName());
                student.get(index); // Suspend it...
                // Find all the competency used for each job field
            }
        });
        return  null;
    }

    private Integer findPositionStudentCompetency(List<Competency> student, String competencyName){
        int index = 0;
        for (Competency competency : student){
            if (competency.getCompetencyName().equalsIgnoreCase(competencyName))
                return index;
            index++;
        }
        return null;
    }
    
    private Map<String,List<Competency>> listCompetencyAndScore (List<JobFieldEntity> jobFieldList){
        Map<String,List<Competency>> competencyAndScore = new TreeMap<>();
        for (JobFieldEntity jf : jobFieldList){
            // Condition of Throwing back 6 months
            List<JobEntity> jobs = jobRepository.findAllByJobFieldEntity(jf);
            List<Competency> competencies = listCompetencyFromJob(jobs);
            competencyAndScore.put(jf.getJobFieldName(),competencies);
        }
        return competencyAndScore;
    }
    
    private List<Competency> listCompetencyFromJob(List<JobEntity> jobs){
        List<Competency> competencies = new ArrayList<>();
        for(JobEntity job : jobs){
            for(JobCompetenciesEntity jobCompetenciesEntity: job.getJobCompetencies()) {
                Competency competency = new Competency();
                competency.setCompetencyName(jobCompetenciesEntity.getCompetenciesEntity().getName());
                competency.setScore(jobCompetenciesEntity.getWeightScore());
                Integer index = isContainName(competencies,competency);
                if (index != null) {
                    double oldScore = competencies.get(index).getScore();
                    competencies.get(index).setScore(oldScore + competency.getScore());
                }
                else
                    competencies.add(competency);
            }
        }
        return competencies;
    }

    private Integer isContainName(List<Competency> competencies, Competency competency){
        for (int i=0; i<competencies.size(); i++){
            if(competencies.get(0).getCompetencyName().equalsIgnoreCase(competency.getCompetencyName()))
                return i;
        }
        return null;
    }
    
    private Map<String, List<Competency>> normalizeToPercent(Map<String, List<Competency>> competencyAndScore){

        competencyAndScore.forEach((k,v) ->  {
            double total = sumScoreInList(v);
            for(Competency competency : v){
                double percent = (competency.getScore()/total)*100;
                competency.setScore(percent);
            }
        });
        return competencyAndScore;
    }

    private double sumScoreInList(List<Competency> competencies){
        double total = 0;
        for (Competency competency : competencies){
            total+=competency.getScore();
        }
        return total;
    }

}
