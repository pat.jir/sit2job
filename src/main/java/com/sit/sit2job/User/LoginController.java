package com.sit.sit2job.User;

import com.sit.sit2job.Company.Repository.UserRepository;
import com.sit.sit2job.User.Entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class LoginController {

    @Autowired
    UserRepository userRepository;

    @GetMapping(value="/login")
    public ModelAndView login(ModelAndView modelAndView){
        modelAndView.setViewName("/login");
        return modelAndView;
    }

    @GetMapping(value = "/successLogin")
    public Object loginSuccess(Principal principal){
        UserEntity userEntity = userRepository.findOneByUsername(principal.getName());
        if(userEntity.getRoleId().getRoleName().equalsIgnoreCase("company"))
            return "redirect:/job/";
        else
            return "redirect:/student/";
    }

    @GetMapping(value = "/failLogin")
    public Object loginfail(){
        return "redirect:/login";
    }

}
