package com.sit.sit2job.Company.POJO;


import java.util.Map;

public class WeightJobCompetency {

    private Map<String,Integer> weightScore;

    public Map<String, Integer> getWeightScore() {
        return weightScore;
    }

    public void setWeightScore(Map<String, Integer> weightScore) {
        this.weightScore = weightScore;
    }
}
