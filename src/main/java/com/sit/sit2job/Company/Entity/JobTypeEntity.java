package com.sit.sit2job.Company.Entity;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "job_type", schema = "sit2job")
public class JobTypeEntity {
    private int jobTypeId;
    private String jobType;
    private List<JobEntity> jobEntity;

    @Id
    @Column(name = "job_type_id", nullable = false)
    public int getJobTypeId() {
        return jobTypeId;
    }

    public void setJobTypeId(int jobTypeId) {
        this.jobTypeId = jobTypeId;
    }

    @Basic
    @Column(name = "job_type", nullable = false, length = 20)
    public String getJobType() {
        return jobType;
    }

    public void setJobType(String jobType) {
        this.jobType = jobType;
    }

    @OneToMany(mappedBy = "jobTypeEntity")
    public List<JobEntity> getJobEntity() {
        return jobEntity;
    }

    public void setJobEntity(List<JobEntity> jobEntity) {
        this.jobEntity = jobEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JobTypeEntity that = (JobTypeEntity) o;
        return jobTypeId == that.jobTypeId &&
                Objects.equals(jobType, that.jobType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(jobTypeId, jobType);
    }
}
