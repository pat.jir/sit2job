package com.sit.sit2job.Student.POJO;

import java.util.Map;

public class SITCoinValue {

    private String userId;
    private Map<String,Double> competencies;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Map<String, Double> getCompetencies() {
        return competencies;
    }

    public void setCompetencies(Map<String, Double> competencies) {
        this.competencies = competencies;
    }
}
